from django.shortcuts import render

# Create your views here.

from .models import Zona
from rest_framework import viewsets
from .serializers import ZonaSerializer
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.http import JsonResponse



class ZonaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows zonas to be viewed or edited.
    """
    queryset = Zona.objects.all().order_by('tipo')
    serializer_class = ZonaSerializer

    def get_queryset(self):
        longitude = self.request.query_params.get('lon')
        latitude= self.request.query_params.get('lat')
        location = Point(float(longitude), float(latitude))
        zonas = Zona.objects.filter(ubicacion__distance_lt=(location, Distance(km=3)))
        return zonas