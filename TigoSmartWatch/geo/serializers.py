from .models import Zona
from rest_framework import serializers


class ZonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Zona
        fields = ['tipo', 'ubicacion', 'radio', 'nivel']
