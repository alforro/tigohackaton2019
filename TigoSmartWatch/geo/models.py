from django.contrib.gis.db import models

# Create your models here.

TIPO = [
    ('inun', 'Inundacion'),
    ('ince', 'Incendios'),
    ('robo', 'Posible Robo'),
    ('peli', 'Zona Peligrosa'),
]

NIVEL = [
    ('bajo', 'Bajo'),
    ('medio', 'Medio'),
    ('alto', 'Alta')
]

class Zona(models.Model):
    tipo = models.CharField(choices=TIPO, max_length=4)
    ubicacion = models.PointField()
    expiracion = models.IntegerField(default=5)
    activo = models.BooleanField(default=True)
    radio = models.PositiveIntegerField(default=1)
    nivel = models.CharField(choices=NIVEL, max_length=5)
