from django.contrib import admin
# Register your models here.


from django.contrib.gis.admin import OSMGeoAdmin
from .models import Zona

@admin.register(Zona)
class ZonaAdmin(OSMGeoAdmin):
    list_display = ('tipo', 'ubicacion')
